package Validations;

import Reflection.ProceeduralController;
import RuntimeParameters.*;
import SeleniumDrivers.ChromeWebDriver;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ezDriver_Selenium_Tests {

    //TODO: store configuration and datastore objects to storage as json
    //TODO: populate datastore and configuration objects from storage using json

    @Test
    public void testIntegration() {
        //TODO: drive webDriver instantiation through configuration data (environment and target browsers)
        //TODO: allow datastore objects to fetch instantiated webDriver object instance
        //TODO: webDriver should stay in memory during testcase, suite, or project execution (maybe allow this to be configured in the dataDriverConfiguration?)
        org.openqa.selenium.WebDriver webDriver = ChromeWebDriver.getWebDriver();

        //this class will be populated from persistent data
        DataDriveConfiguration dataDriveConfiguration = getDataDriveConfigurationExampleIObject();

        //this is pretty close to what we expect the entry point to call
        ProceeduralController.run(dataDriveConfiguration, new Object[]{webDriver});
    }

    @Test
    public void DataDriveConfigurationToJsonUsingJackson(){
        DataDriveConfiguration dataDriveConfiguration = getDataDriveConfigurationExampleIObject();
        String json = GenerateJsonConfigurationFromDataDriveConfiguration(dataDriveConfiguration);

        Assert.assertNotNull(json);
    }

    private String GenerateJsonConfigurationFromDataDriveConfiguration(DataDriveConfiguration dataDriveConfiguration) {
        //example of using Jackson to generate json from objects
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(dataDriveConfiguration);
            System.out.println("JSON = " + json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return json;
    }

    private DataDriveConfiguration getDataDriveConfigurationExampleIObject() {
        //building the configuration object in code instead of loading from json
        DataDriveConfiguration dataDriveConfiguration = new DataDriveConfiguration("J&T Productions", "J&T");
        Project project = new Project("ezDriver", "DEVELOP", "Smoke", "1.0");
        Suite suite = new Suite("Demo suite");
        TestCase testCase = new TestCase("Example Page Test");
        Step step1 = new Step(1, "Login Page", "userName", "SendKeys", new ArrayList<String>(Arrays.asList("Hello James!")));

        testCase.getSteps().put(step1.getStepNumber(), step1);
        suite.getTestCases().put(testCase.getName(), testCase);
        project.getSuites().put(suite.getName(),suite);
        dataDriveConfiguration.getProjects().put(project.getName(), project);

        return dataDriveConfiguration;
    }
}
