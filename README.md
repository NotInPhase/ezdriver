# ezDriver


//TODO: rewrite readme to detail API purpose, usage, author and crediting information. 

(NOT FULLY DOCUMENTED, PIECES MISSING!!!!!)

Once complete the following tasks should allow for the full data driving of any application not just automation/selenium specific code/ design patterns
        it should be cautioned that this concept invites anyone to manipulate the execution flow and output of data and should be held in mind during development to prevent improper usage of the application and data contained where in.
        as developers we may control what and how data is exposed to end-users
        (this is probably all sorts of frowned upon not sure -_(-_-)_-
        Creating the API, data store, and UX will be the forefront of work effort to begin this project

        in the interest of organization and understanding lets coin some terms specific to this document
        parent project - refers to the project that this api is imported into
        configuration data - refers to the data provided to a parent projects arguments/runtime to control the procedural execution of that application dynamically


        by porting class names and their relative members to a data store we may have reference to the accessible functions of an application when compiling application arguments
        TODO: figure a way to iterate packages and compile lists of available non static and static class names + port lists to dataStore

        TODO: figure a way to iterate classes by name and compile a list of available methods for both instance and static methods + port lists to dataStore

        TODO: figure a way to iterate classes by name and compile a list of accessible fields and properties + port lists to dataStore

        NOTE: it may be more reasonable/stable to create custom annotations for creating/mapping dataStore objects, rather than iterating using reflection, this could allow developers to more securely control exposure and easily assign aliases

        TODO: class and member lists should be loaded dynamically at runtime of parent projects to allow for fast failures when given references no longer exist in code
        this could also lend itself to allowing the ux to know when a particular configuration is broken in real time

        TODO: it is possible to create a consumable output that would allow configuration data to automatically update when developers make new releases

        TODO: MOVE data compilation methods to self class/package

        TODO: MOVE data compilation method calls to initialization of application

        TODO: MOVE class loaders and manipulators methods to self class/package

        TODO: MOVE class loaders and manipulators to Controller Class in parent project (e.g. automation framework)

        TODO: figure a way to call class members both static and non-static using reflection passing arguments when needed

        TODO: return generic class instance and figure a way to call instance members using reflection

        TODO: Handle all exceptions from application handlers
        TODO: Track all usage for later logging and debugging, this tracking may also allow for security checks before execution of malicious configurations.



        A rough idea of a parent applications entry point might look like this


        Main(args)
            if(args == updateDataStores)
                PopulateDataStores()
             else
                LoadMemberDefinitions()
                LoadConfigurationDataJSON() //i think json would be a great way to load the application data
                foreach step in JSON_Steps
                    step.ValidateConfiguration(); //validateConfiguration method would have access to memberDefinition objects for fast fail
                    GetClassData(step.ClassName);
                    if(classData.ClassType.isStatic)
                        GetClass
                    else*
                        GetClassInstance



