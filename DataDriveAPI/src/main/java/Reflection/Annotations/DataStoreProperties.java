package Reflection.Annotations;

import java.lang.annotation.*;

public interface DataStoreProperties extends Annotation {

    //TODO: flesh out appropriate annotations and properties to allow for the construction of proper DataStore objects

    /* still working on this. need to determine what all data we want to store and make the methods to do so... using reflection
     These annotations will be used to populate the DataStoreObjects
     Once the DataStoreObjects have been populated we can port them out to a storage in Json format, we will then be able to read and write to the storage for any changes/updates
     DataStoreObjects will be used to query information relevant to reflection calls
     we can use reverse lookup mechanisms to query information from the DataStoreObjects based on limited input from configuration parameters
     this will reduce the number of input parameters required to drive the runtime procedures and allow for more intelligent input */

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface Init {
    }

    //TODO: fix naming conventions across the code base (e.g. POM is too specific here as this annotation may be applied to any class)

    //        TYPE, //If you want to annotate class, interface, enum..
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface POM {
        String name() default "";
    }

    //        CONSTRUCTOR, //If you want to annotate constructor
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.CONSTRUCTOR)
    public @interface DataStoreConstructor{
        String name() default "";
    }

    //        METHOD, //If you want to annotate method
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface DataStoreMethod{
        String name() default "";
    }

    //        METHOD, //If you want to annotate method
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface Action {
        String name() default "";
    }

    //        METHOD, //If you want to annotate method
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface Keyword{
        String name() default "";
    }

    //        FIELD, //If you want to annotate field (includes enum constants)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface DataStoreField{
        String name() default "";
    }

    //        PARAMETER, //If you want to annotate parameter
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface DataStoreParameter{

    }




//    public enum ElementType {
//        TYPE, //If you want to annotate class, interface, enum..
//        FIELD, //If you want to annotate field (includes enum constants)
//        METHOD, //If you want to annotate method
//        PARAMETER, //If you want to annotate parameter
//        CONSTRUCTOR, //If you want to annotate constructor
//        LOCAL_VARIABLE, //..
//        ANNOTATION_TYPE, //..
//        PACKAGE, //..
//        TYPE_PARAMETER, //..(java 8)
//        TYPE_USE; //..(java 8)
//
//        private ElementType() {
//        }

//          @Retention: annotation indicates how the custom annotation is stored. There are 3 types of retention.
//              SOURCE — analyses by compiler and never stored
//              CLASS — stored into class file and not retained in runtime
//              RUNTIME — store into class file and usable in runtime(by reflection)


}
