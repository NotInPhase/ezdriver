package Pages;

import Reflection.Annotations.DataStoreProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

@DataStoreProperties.POM(name = "Login Page")
public class ExamplePage extends BasePageObjectModelImpl {

    @DataStoreProperties.DataStoreField
    @FindBy(id = "user_login")
    public WebElement userName;

    @DataStoreProperties.DataStoreField
    @FindBy(xpath = ".//*[@id='user_pass']")
    public WebElement password;

    @DataStoreProperties.DataStoreField
    @FindBy(css = "#login")
    public WebElement loginButton;

    @DataStoreProperties.DataStoreConstructor
    public ExamplePage(WebDriver driver) {
        super(driver);
    }

//    @DataStoreProperties.DataStoreConstructor
//    public ExamplePage() {
//        super();
//    }

    @DataStoreProperties.DataStoreMethod
    public void typeUserName(String value){
        userName.sendKeys(value);
    }

    @DataStoreProperties.DataStoreMethod
    public void clickOnLoginButton() {
        MoveTo(loginButton);
        Click(loginButton);
    }

}
