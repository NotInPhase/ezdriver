package propertyhandler;

import map.MapOfStrings;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

enum PropertyHandlerImpl implements PropertyHandler {
    SINGLETON;

    private final String CONFIGURATION_PATH = "configuration/";
    private final String PROPERTIES_EXTENSION = ".properties";
    private final String GLOBAL_PROPERTIES_FILENAME= "global.properties";
    private final String ENVIRONMENT_PATH = "environments/";
    private final String ENV_VARIABLE_MARKER_START = "{$";
    private final String ENV_VARIABLE_MARKER_END = "}";
    private final String ACTIVE_ENV_OVERRIDE_VAR = "env";
    private MapOfStrings globalPropertyMap = this.getMergedProperties(PropertyType.GLOBAL);
    private MapOfStrings environmentPropertyMap = this.getMergedProperties(PropertyType.ENVIRONMENT);
    private MapOfStrings dataManagerPropertyMap = this.getMergedProperties(PropertyType.DATA_MANAGER);

    private MapOfStrings getMergedProperties(PropertyHandlerImpl.PropertyType propertyType) {
        MapOfStrings mergedPropertyMap = this.readPropertyFile(propertyType, PropertyLevel.COMMON);
        mergedPropertyMap.putAll(this.readPropertyFile(propertyType, PropertyLevel.PROJECT_SPECIFIC));
        return mergedPropertyMap;
    }

    private MapOfStrings readPropertyFile(PropertyType propertyType, PropertyLevel propertyLevel) {
        MapOfStrings propertyMap = new MapOfStrings();
        Properties prop = new Properties();
        InputStream fileInputStream = null;
        String propertyFileName = this.getPropertyFileName(propertyType);
        if (propertyFileName != null) {
            try {
                switch (propertyLevel) {
                    case PROJECT_SPECIFIC:
                        try {
                            fileInputStream = new FileInputStream(propertyFileName);
                        } catch (FileNotFoundException ex) {
                        }
                        break;
                    case COMMON:
                        fileInputStream = this.getClass().getResourceAsStream("/" + propertyFileName);
                        break;
                    default:
                        throw new RuntimeException("Invalid property level.");
                }

                if (fileInputStream != null) {
                    prop.load(fileInputStream);

                    String key;
                    String value;
                    for (Iterator propertyNames = prop.stringPropertyNames().iterator(); propertyNames.hasNext(); propertyMap.put(key, value)) {
                        key = (String) propertyNames.next();
                        value = prop.getProperty(key);
                        if (!StringUtils.isEmpty(value) && value.contains(ENV_VARIABLE_MARKER_START) && value.contains(ENV_VARIABLE_MARKER_END)) {
                            value = this.expandEnvironmentVariable(key, value);
                        }
                    }

                    fileInputStream.close();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        return propertyMap;
    }

    public MapOfStrings getGlobalPropertyMap() {
        return this.globalPropertyMap;
    }

    public MapOfStrings getEnvironmentPropertyMap() {
        return this.environmentPropertyMap;
    }

    public MapOfStrings getDataManagerPropertyMap() {
        return this.dataManagerPropertyMap;
    }

    public String getEnvironmentProperty(String key) {
        return this.environmentPropertyMap.get(key);
    }

    public String getGlobalProperty(String key) {
        return this.globalPropertyMap.get(key);
    }

    public String getDataManagerProperty(String key) {
        return this.dataManagerPropertyMap.get(key);
    }

    public void setGlobalProperty(String key, String value) {
        try {
            PropertiesConfiguration props = new PropertiesConfiguration(CONFIGURATION_PATH + GLOBAL_PROPERTIES_FILENAME);
            props.setProperty(key, value);
            props.save();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        this.refreshProperties();
    }

    public void updateEnvironmentProperty(String key, String value) {
        this.environmentPropertyMap.put(key, value);
    }

    private void refreshProperties() {
        this.globalPropertyMap = this.getMergedProperties(PropertyType.GLOBAL);
        this.environmentPropertyMap = this.getMergedProperties(PropertyType.ENVIRONMENT);
    }

    private String getPropertyFileName(PropertyType propertyType) {
        String propertyFileName = CONFIGURATION_PATH;
        switch (propertyType) {
            case GLOBAL:
                propertyFileName = propertyFileName + GLOBAL_PROPERTIES_FILENAME;
                break;
            case ENVIRONMENT:
                propertyFileName = propertyFileName + ENVIRONMENT_PATH + this.getActiveEnvironment() + PROPERTIES_EXTENSION;
                break;
            case DATA_MANAGER:
                if (this.getActiveDataManager() != null) {
                    propertyFileName = propertyFileName + ENVIRONMENT_PATH + this.getActiveDataManager() + PROPERTIES_EXTENSION;
                } else {
                    propertyFileName = null;
                }
                break;
            default:
                throw new RuntimeException("Invalid property type.");
        }
        return propertyFileName;
    }

    private String getActiveEnvironment() {
        if (StringUtils.isNotEmpty(System.getenv(ACTIVE_ENV_OVERRIDE_VAR))) {
            return System.getenv(ACTIVE_ENV_OVERRIDE_VAR);
        } else if (StringUtils.isNotEmpty(this.getGlobalPropertyMap().get("environment"))) {
            return this.getGlobalPropertyMap().get("environment");
        } else {
            throw new RuntimeException("Invalid environment configured. Check the environment property in " + GLOBAL_PROPERTIES_FILENAME);
        }
    }

    private String getActiveDataManager() {
        return this.getEnvironmentPropertyMap().get("dataManager") != null ? this.getGlobalPropertyMap().get("dataManager") : null;
    }

    private String expandEnvironmentVariable(String key, String value) {
        String variableName = StringUtils.substringBetween(value, ENV_VARIABLE_MARKER_START, ENV_VARIABLE_MARKER_END);
        String variableValue = System.getProperty(variableName);
        if (StringUtils.isEmpty(variableValue) || variableValue.equalsIgnoreCase("null")) {
            variableValue = System.getenv(variableName);
            if (StringUtils.isEmpty(variableValue) || variableValue.equalsIgnoreCase("null")) {
                throw new RuntimeException(String.format("Unable to expand system variable ('%s') in property value ('%s'). Check your system settings to ensure the variable is defined.", variableName, key));
            }
        }

        System.out.println();
        return value.replace(ENV_VARIABLE_MARKER_START + variableName + ENV_VARIABLE_MARKER_END, variableValue);
    }

    private enum PropertyLevel {
        COMMON,
        PROJECT_SPECIFIC;

        PropertyLevel() {
        }
    }

    private enum PropertyType {
        GLOBAL,
        ENVIRONMENT,
        DATA_MANAGER;

        PropertyType() {
        }
    }
}
