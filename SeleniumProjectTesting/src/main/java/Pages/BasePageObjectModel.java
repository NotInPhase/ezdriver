package Pages;

import org.openqa.selenium.WebElement;

public interface BasePageObjectModel {

    void Click(WebElement webElement);
    void MoveTo(WebElement webElement);
    void SendKeys(WebElement webElement, String value);
    void Clear(WebElement webElement);
}
