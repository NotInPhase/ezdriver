package DataStoreObjects;

import Reflection.Annotations.DataStoreProperties;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Parameter;
import java.util.HashMap;

public class Method {
    public String name;
    public String alias = "";
    public boolean isParameterized = false;
    public int parameterCount;
    public HashMap<String, ParameterInfo> parameters = new HashMap<>();

    public Method(java.lang.reflect.Method method) {
        this.name = method.getName();
        setAlias(method);
        setParameterInfo(method);
    }

    private void setParameterInfo(java.lang.reflect.Method method) {
        if((parameterCount = method.getParameterCount()) > 0){
            isParameterized = true;
        }
        for (Parameter parameter : method.getParameters()) {
            parameters.put(parameter.getName(), new ParameterInfo(parameter));
        }
    }

    private void setAlias(java.lang.reflect.Method method) {
        this.alias = method.getName();
        if(method.isAnnotationPresent(DataStoreProperties.Action.class)) {
            DataStoreProperties.Action action = method.getAnnotation(DataStoreProperties.Action.class);
            if(StringUtils.isNotEmpty(action.name()))
                alias = action.name();
            else
                alias = method.getName();
        }

        if(method.isAnnotationPresent(DataStoreProperties.Keyword.class)){
            DataStoreProperties.Keyword keyword = method.getAnnotation(DataStoreProperties.Keyword.class);
            if(StringUtils.isNotEmpty(keyword.name()))
                alias = keyword.name();
            else
                alias = method.getName();
        }
    }
}
