package Pages;

import Reflection.Annotations.DataStoreProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import propertyhandler.PropertyHandler;

@DataStoreProperties.POM
public class BasePageObjectModelImpl implements BasePageObjectModel {

    public static PropertyHandler propertyHandler = PropertyHandler.IMPL;

    @DataStoreProperties.DataStoreField
    public WebDriver webDriver;
            //= SeleniumDrivers.WebDriver.getWebDriver();

    @DataStoreProperties.DataStoreConstructor
    public BasePageObjectModelImpl(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
//    @DataStoreProperties.DataStoreConstructor
//    public BasePageObjectModelImpl(){
//        PageFactory.initElements(webDriver, this);
//    }

    @Override
    @DataStoreProperties.DataStoreMethod
    public void Click(WebElement webElement) {
        webElement.click();
    }

    @Override
    @DataStoreProperties.DataStoreMethod
    public void MoveTo(WebElement webElement) {
        Actions a = new Actions(webDriver);
        a.moveToElement(webElement).build().perform();
    }

    @Override
    @DataStoreProperties.DataStoreMethod
    public void SendKeys(WebElement webElement, String value) {
        webElement.sendKeys(value);
    }

    @Override
    @DataStoreProperties.DataStoreMethod
    public void Clear(WebElement webElement) {
        webElement.clear();
    }
}
