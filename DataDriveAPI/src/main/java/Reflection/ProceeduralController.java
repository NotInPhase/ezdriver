package Reflection;

import DataStoreObjects.ClassInfo;
import DataStoreObjects.DataStore;
import RuntimeParameters.*;

public class ProceeduralController {
    private static DataStore dataStore = new DataStore();
    private static ReflectionMethods reflectionMethods = new ReflectionMethods();


    //TODO: make an interface and try to remove dependencies for automation practices. Overall the objects we should be expecting should follow a nested hierarchy structure
    //TODO: at least with an interface we may describe what is needed for those who want to create their own controllers, the controllers with the api should be generic

    //TODO: Make the webdriver so it doesnt have to be passed and instead fetched via classinfo object when required as input to a method or constructor
    public static void run(DataDriveConfiguration dataDriveConfiguration, Object[] constructorParameters){
        //based on dataDriveConfigurations and dataStore information determine the call sequence of reflection methods
        for (Project project : dataDriveConfiguration.getProjects().values()) {
            for (Suite suite : project.getSuites().values()) {
                for (TestCase testCase : suite.getTestCases().values()) {
                    for (Step step : testCase.getSteps().values()) {

                        //TODO: do stuff with miscellaneous data from configuration (add to logging objects to fluff testing details)

                        //TODO: Make classInfo objects more robust in their ability to answer the reflection router questions and place parameters

                        //TODO: extract this into reflectionRouter method
                        String pageName = dataStore.pageAliasLookup.get(step.getPage());
                        ClassInfo classInfo = dataStore.classes.get(pageName);
                        if(!classInfo.isStatic && classInfo.methods.get(step.getAction()).isParameterized){
                            //answer questions about constructor // determine which constructor to use if multiple????
                            Object classInstance = reflectionMethods.InstantiateClass(classInfo.name, constructorParameters, (Class<?>)classInfo.constructors.get(pageName).parameters.get("driver").parameterType);
                            reflectionMethods.InvokeInstanceMethodFromClass(classInstance, step.getAction(), new Object[]{ reflectionMethods.GetField(classInstance, classInfo.fields.get(step.getElement()).name), step.getParameters().get(classInfo.methods.get(step.getAction()).parameterCount-2) } );
                        }
                    }
                }
            }
        }
    }

    //****OLD****
    //this is a very toned down version of the controller these parameters will eventually come from the dataDriveConfiguration object
    public static void run(String page, String action, String Keyword, Object[] constructorParameters, Object[] parameters){
        String pageName = dataStore.pageAliasLookup.get(page);
        ClassInfo classInfo = dataStore.classes.get(pageName);
        if(!classInfo.isStatic && classInfo.methods.get(action).isParameterized){
            //answer questions about constructor // determine which constructor to use if multiple????
            Object classInstance = reflectionMethods.InstantiateClass(classInfo.name, constructorParameters, (Class<?>)classInfo.constructors.get(pageName).parameters.get("driver").parameterType);
            reflectionMethods.InvokeInstanceMethodFromClass(classInstance, action, new Object[]{ reflectionMethods.GetField(classInstance, classInfo.fields.get("userName").name), parameters[0] } );
        }
    }
}
