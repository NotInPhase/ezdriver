package map;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapOfStrings extends LinkedHashMap<String, String> {
    public MapOfStrings(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public MapOfStrings(int initialCapacity) {
        super(initialCapacity);
    }

    public MapOfStrings() {
    }

    public MapOfStrings(int initialCapacity, float loadFactor, boolean accessOrder) {
        super(initialCapacity, loadFactor, accessOrder);
    }

    public MapOfStrings(Map<? extends String, ?> mapOfObjects) {
        Iterator var2 = mapOfObjects.entrySet().iterator();

        while (var2.hasNext()) {
            Entry<? extends String, ?> entry = (Entry) var2.next();
            this.put(entry.getKey(), entry.getValue());
        }
    }

    public void put(String key, Object value) {
        this.put(key, value == null ? null : value.toString());
    }

}
