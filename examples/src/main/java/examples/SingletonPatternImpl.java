package examples;

enum SingletonPatternImpl implements SingletonPattern{
    SINLETON;

    private final String myConstant = "";
    public static String myString = "";

    public void doSomething(){
        //you logic here
    }
}
