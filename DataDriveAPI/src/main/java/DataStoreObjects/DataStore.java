package DataStoreObjects;

import Reflection.Annotations.AnnotationProcessorUtil;
import Reflection.Annotations.DataStoreProperties;
import org.apache.commons.lang3.StringUtils;
import propertyhandler.PropertyHandler;

import java.util.HashMap;
import java.util.List;

//TODO: need more analysis of required data and structure of DataStore objects
public class DataStore extends AnnotationProcessorUtil {
    public HashMap<String, ClassInfo> classes = new HashMap<>();

    //example reverse lookup mechanism
    public HashMap<String, String> pageAliasLookup = new HashMap<>();
    PropertyHandler propertyHandler = PropertyHandler.IMPL;
    //TODO: add hashmaps having keys of methods/actions/keywords/elements/fields aliases and having string values of the class names they are found in to be able to reverse lookup keys for the classes hashmap and be able to return a single class from classes

    public DataStore() {
        String packagesString = propertyHandler.getEnvironmentProperty("dataStorePackages");
        processPackages(packagesString);
    }

    public DataStore(String scanningPackage) {
        processPackages(scanningPackage);
    }

    public DataStore(String[] scanningPackages) {
        processPackages(scanningPackages);
    }

    public void processPackages(List<String> pkgNames) {
        for (String pkgName : pkgNames) {
            processPackages(pkgName);
        }
    }

    public void processPackages(String[] pkgNames) {
        for (String pkgName : pkgNames) {
            processPackages(pkgName);
        }
    }

    private void processPackages(String packages) {
        String[] pkgNames = packages.split(",");
        for (String pkgname : pkgNames) {
            for (Class<?> clazz : getClassesInPackage(packages)) {
                if (clazz.isAnnotationPresent(DataStoreProperties.POM.class)) {
                    DataStoreProperties.POM pom = clazz.getAnnotation(DataStoreProperties.POM.class);
                    if(StringUtils.isNotEmpty(pom.name()))
                        pageAliasLookup.put(pom.name(), clazz.getName());
                    else
                        pageAliasLookup.put(clazz.getSimpleName(), clazz.getName());
                    this.classes.put(clazz.getName(), new ClassInfo(clazz));
                }
            }
        }
    }

}
