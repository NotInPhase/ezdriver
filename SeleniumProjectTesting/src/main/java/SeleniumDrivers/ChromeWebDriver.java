package SeleniumDrivers;

import org.openqa.selenium.chrome.ChromeDriver;
import propertyhandler.PropertyHandler;

public class ChromeWebDriver {

    public static PropertyHandler propertyHandler = PropertyHandler.IMPL;
    public static final String CHROME_DRIVER_PROPERTY_KEY = "webdriver.chrome.driver";
    public static final String DEMO_SITE_PROPERTY_KEY = "demoSite";

    public static org.openqa.selenium.WebDriver getWebDriver(){
        System.setProperty(CHROME_DRIVER_PROPERTY_KEY, propertyHandler.getEnvironmentProperty(CHROME_DRIVER_PROPERTY_KEY));
        org.openqa.selenium.WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.get(propertyHandler.getEnvironmentProperty(DEMO_SITE_PROPERTY_KEY));
        return webDriver;
    }
}
