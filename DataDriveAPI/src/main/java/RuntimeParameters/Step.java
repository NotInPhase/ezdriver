package RuntimeParameters;

import java.util.ArrayList;
import java.util.List;

public class Step {

    private Integer stepNumber = 0;
    private String page = null;
    private String element = null;
    private String action = null;
    private List<String> parameters = new ArrayList<String>();

    public Step() {
    }

    public Step(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public Step(Integer stepNumber, String page) {
        this.stepNumber = stepNumber;
        this.page = page;
    }

    public Step(Integer stepNumber, String page, String element) {
        this.stepNumber = stepNumber;
        this.page = page;
        this.element = element;
    }

    public Step(Integer stepNumber, String page, String element, String action) {
        this.stepNumber = stepNumber;
        this.page = page;
        this.element = element;
        this.action = action;
    }

    public Step(Integer stepNumber, String page, String element, String action, List<String> parameters) {
        this.stepNumber = stepNumber;
        this.page = page;
        this.element = element;
        this.action = action;
        this.parameters = parameters;
    }

    public void setDefaults(){
        this.stepNumber = 0;
        this.page = "";
        this.element = "";
        this.action = "";
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }
}
