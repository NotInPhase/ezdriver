package DataStoreObjects;

import java.lang.reflect.Type;

public class Field {
    public String name;
    public Object value;
    public Type type;


    public Field(java.lang.reflect.Field field) {
        this.name = field.getName();
        try {
            this.value = field.get(field.getDeclaringClass().getClass());
        } catch (Exception ex){

        }
        this.type = field.getType();
    }
}
