package RuntimeParameters;

import java.util.HashMap;

public class TestCase {
    private String name;
    private HashMap <Integer, Step> steps = new HashMap<>();

    public TestCase(){
    }

    public TestCase(String name) {
        this.name = name;
    }

    public TestCase(String name, HashMap<Integer, Step> steps) {
        this.name = name;
        this.steps = steps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<Integer, Step> getSteps() {
        return steps;
    }

    public void setSteps(HashMap<Integer, Step> steps) {
        this.steps = steps;
    }
}
