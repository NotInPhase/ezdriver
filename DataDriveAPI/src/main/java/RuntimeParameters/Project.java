package RuntimeParameters;

import java.util.HashMap;

public class Project {
    private String name;
    private String environment;
    private String execution_type;
    private String release_version;
    private HashMap<String, Suite> suites = new HashMap<>();

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String environment) {
        this.name = name;
        this.environment = environment;
    }

    public Project(String name, String environment, String execution_type) {
        this.name = name;
        this.environment = environment;
        this.execution_type = execution_type;
    }

    public Project(String name, String environment, String execution_type, String release_version) {
        this.name = name;
        this.environment = environment;
        this.execution_type = execution_type;
        this.release_version = release_version;
    }

    public Project(String name, String environment, String execution_type, HashMap<String, Suite> suites) {
        this.name = name;
        this.environment = environment;
        this.execution_type = execution_type;
        this.suites = suites;
    }

    public Project(String name, String environment, String execution_type, String release_version, HashMap<String, Suite> suites) {
        this.name = name;
        this.environment = environment;
        this.execution_type = execution_type;
        this.release_version = release_version;
        this.suites = suites;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getExecution_type() {
        return execution_type;
    }

    public void setExecution_type(String execution_type) {
        this.execution_type = execution_type;
    }

    public String getRelease_version() {
        return release_version;
    }

    public void setRelease_version(String release_version) {
        this.release_version = release_version;
    }

    public HashMap<String, Suite> getSuites() {
        return suites;
    }

    public void setSuites(HashMap<String, Suite> suites) {
        this.suites = suites;
    }
}
