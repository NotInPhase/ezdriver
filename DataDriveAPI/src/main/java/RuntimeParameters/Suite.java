package RuntimeParameters;

import java.util.HashMap;

public class Suite {
    private String name;
    private HashMap<String, TestCase> testCases = new HashMap<>();

    public Suite(String name) {
        this.name = name;
    }

    public Suite(String name, HashMap<String, TestCase> testCases) {
        this.name = name;
        this.testCases = testCases;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(HashMap<String, TestCase> testCases) {
        this.testCases = testCases;
    }
}
