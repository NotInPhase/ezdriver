package RuntimeParameters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class DataDriveConfiguration {
    //i think this drive configuration would actually be described in the project, not this api. it is possible that we may be able to give a base interface and implementation for those who are either lazy or unknowing.
    private final DateTimeFormatter DATETIMEFORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd - HH:mm:ss");
    private final LocalDateTime LOCALDATETIME = LocalDateTime.now();

    private String companyName;
    private String teamName;
    private String dateTimeNow = DATETIMEFORMATTER.format(LOCALDATETIME);
    private HashMap<String, Project> projects = new HashMap<>();

    public DataDriveConfiguration(String companyName, String teamName) {
        this.companyName = companyName;
        this.teamName = teamName;
    }

    public DataDriveConfiguration(String companyName, String teamName, HashMap<String, Project> projects) {
        this.companyName = companyName;
        this.teamName = teamName;
        this.projects = projects;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getDateTimeNow() {
        return dateTimeNow;
    }

    public void setDateTimeNow(String dateTimeNow) {
        this.dateTimeNow = dateTimeNow;
    }

    public HashMap<String, Project> getProjects() {
        return projects;
    }

    public void setProjects(HashMap<String, Project> projects) {
        this.projects = projects;
    }
}
