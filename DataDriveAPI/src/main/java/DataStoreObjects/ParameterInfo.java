package DataStoreObjects;

import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

public class ParameterInfo {
    public String parameterName;
    public Type parameterType;

    public ParameterInfo(Parameter parameter) {
        this.parameterName = parameter.getName();
        this.parameterType = parameter.getType();
    }
}
