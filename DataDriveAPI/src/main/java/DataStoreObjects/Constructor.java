package DataStoreObjects;

import java.lang.reflect.Parameter;
import java.util.HashMap;

public class Constructor {

    public String name;
    public boolean isParameterized = false;
    public int parameterCount;
    public HashMap<String, ParameterInfo> parameters = new HashMap<>();

    public Constructor(java.lang.reflect.Constructor constructor){
        name = constructor.getName();
        if((parameterCount = constructor.getParameterCount()) > 0){
            isParameterized = true;
        }
        for (Parameter parameter : constructor.getParameters()) {
            parameters.put(parameter.getName(), new ParameterInfo(parameter));
        }
    }
}
