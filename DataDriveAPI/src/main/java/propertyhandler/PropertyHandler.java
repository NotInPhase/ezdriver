package propertyhandler;

import map.MapOfStrings;

public interface PropertyHandler {
    PropertyHandler IMPL = PropertyHandlerImpl.SINGLETON;

    MapOfStrings getGlobalPropertyMap();
    MapOfStrings getEnvironmentPropertyMap();
    String getEnvironmentProperty(String key);
    MapOfStrings getDataManagerPropertyMap();
    String getGlobalProperty(String key);
    String getDataManagerProperty(String key);
    void setGlobalProperty(String key, String value);
    void updateEnvironmentProperty(String key, String value);

    //TODO: make generators that create the base configuration directory and files in the parent project the first time the project is build after importing the api

}
