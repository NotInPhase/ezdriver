package Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionMethods {

    //region Invokers
    public void InvokeInstanceMethodFromClass(Object classInstance, String methodName, Object[] parameters, Class... parameterTypes) {
        try {
            Method method = classInstance.getClass().getMethod(methodName, parameterTypes);
            method.invoke(classInstance, parameters);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void InvokeInstanceMethodFromClass(Object classInstance, String methodName, Object[] parameters) {
        try {
            Method method = classInstance.getClass().getMethod(methodName, GetParameterTypes(parameters));
            method.invoke(classInstance, GetParameterValues(parameters, classInstance));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    //endregion

    //region Getters

    public Object[] GetParameterValues(Object[] parameters, Object classInstance) {
        Object[] result = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (parameters[i].getClass().equals(Field.class)) {
                try {
                    result[i] = ((Field) parameters[i]).get(classInstance);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else
                result[i] = parameters[i];
        }
        return result;
    }

    public Object GetParameterValue(Object parameter, Object classInstance) {
        Object result = null;
        if (parameter.getClass().equals(Field.class)) {
            try {
                result = ((Field) parameter).get(classInstance);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public Object GetField(Object classInstance, String fieldName) {
        Object result = null;
        try {
            Field field = classInstance.getClass().getField(fieldName);
            field.setAccessible(true);
            result = field;
            System.out.println("Public field found: " + result.toString());
        } catch (NoSuchFieldException e) {
            System.out.println(e.toString());
        }
        return result;
    }

    public Object GetField(String className, String fieldName) {
        Object result = null;
        try {
            Field field = GetClassByName(className).getField(fieldName);
            field.setAccessible(true);
            result = field;
            System.out.println("Public field found: " + result.toString());
        } catch (NoSuchFieldException e) {
            System.out.println(e.toString());
        }
        return result;
    }

    public Class<?> GetClassByName(String className) {
        Class<?> result = null;
        try {
            result = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Constructor<?> GetConstructor(Class clazz, Class... parameterTypes) {
        Constructor<?> result = null;
        try {
            result = clazz.getConstructor(parameterTypes);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Constructor<?> GetConstructor(Class clazz, Object[] parameters) {
        Constructor<?> result = null;
        try {
            result = clazz.getConstructor(GetParameterTypes(parameters));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Class[] GetParameterTypes(Object[] parameters) {
        Class[] classes = new Class[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (parameters[i].getClass().equals(Field.class))
                classes[i] = ((Field) parameters[i]).getType();
            else
                classes[i] = parameters[i].getClass();
        }
        return classes;
    }

    public Object GetDynamicClassInstance(Constructor<?> ctor, Object[] parameters) {
        Object result = null;
        try {
            result = ctor.newInstance(parameters);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }

    //endregion

    //region Instantiations

    public Object InstantiateClass(String className, Object[] constructorParameters, Class... constructorParameterTypes) {
        return GetDynamicClassInstance(GetConstructor(GetClassByName(className), constructorParameterTypes), constructorParameters);
    }

    public Object InstantiateClass(String className, Object[] constructorParameters) {
        return GetDynamicClassInstance(GetConstructor(GetClassByName(className), constructorParameters), constructorParameters);
    }


    //endregion

    //TODO: methods for getting fields/parameters from classes
    //TODO: Invoke Static methods
    //TODO: get static fields/parameters from classes
    //TODO: InvokeMethodFromClassWithoutParameters
    //TODO: GetDynamicClassInstanceWithoutConstructorParameters
}
