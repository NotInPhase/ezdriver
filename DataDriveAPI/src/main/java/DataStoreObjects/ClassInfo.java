package DataStoreObjects;

import Reflection.Annotations.DataStoreProperties;

import java.lang.reflect.Type;
import java.util.HashMap;

public class ClassInfo {
    public String name;
    public String alias;
    public Type type;
    public Boolean isStatic = true;
    public HashMap<String, Constructor> constructors = new HashMap<>();
    public HashMap<String, Method> methods = new HashMap<>();
    public HashMap<String, Field> fields = new HashMap<>();

    public ClassInfo(Class<?> clazz) {
        name = clazz.getName();
        type = clazz;
        DataStoreProperties.POM pom = clazz.getAnnotation(DataStoreProperties.POM.class);
        alias = pom.name();

        java.lang.reflect.Constructor<?>[] constructors = clazz.getConstructors();
        if (constructors.length > 0) {
            this.isStatic = false;
            for (java.lang.reflect.Constructor<?> constructor : constructors) {
                if (constructor.isAnnotationPresent(DataStoreProperties.DataStoreConstructor.class)) {
                    constructor.setAccessible(true);
                    this.constructors.put(constructor.getName(), new Constructor(constructor));
                }
            }
        }

        for (java.lang.reflect.Method method : clazz.getMethods()) {
            if (method.isAnnotationPresent(DataStoreProperties.Action.class) ||
            method.isAnnotationPresent(DataStoreProperties.Keyword.class) ||
            method.isAnnotationPresent(DataStoreProperties.DataStoreMethod.class)) {
                method.setAccessible(true);
                this.methods.put(method.getName(), new Method(method));
            }
        }

        for (java.lang.reflect.Field field : clazz.getFields()) {
            if (field.isAnnotationPresent(DataStoreProperties.DataStoreField.class)) {
                field.setAccessible(true);
                this.fields.put(field.getName(), new Field(field));
            }
        }

    }

}
